using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class StartAnimations : MonoBehaviour
{
    [SerializeField] private float _tiempo = 2;
    [SerializeField] private Transform _arrow3D;
    [SerializeField] private Transform _door;
    [SerializeField] private GameObject _bola;
    [SerializeField] private Material _matLaberinto;
    [SerializeField] private Color _color;
    [SerializeField] private Color _color2;

    // Start is called before the first frame update
    public void StartGame()
    {
        _arrow3D.DOScale(new Vector3(0,0,0), _tiempo);
        _door.DOLocalMove(new Vector3(0.213f,0.002f,-0.506f), _tiempo);
        _matLaberinto.DOColor(_color, _tiempo);
    }

    public void RestartGame()
    {
        _arrow3D.DOScale(new Vector3(25,25,25), _tiempo);
        _door.DOLocalMove(new Vector3(0.2135f,-0.0078f,-0.604f), _tiempo);
        _matLaberinto.DOColor(_color2, _tiempo);
        Invoke ("BolaRestart",1.1f);
    }

    private void BolaRestart() {
        _bola.transform.localPosition = new Vector3(0.217f, 0.093f, -0.611f);
        _bola.SetActive(true);
    }
}
