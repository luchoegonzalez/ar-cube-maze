using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TimerScript : MonoBehaviour
{
    public float timer = 0;
    public string segundos = "00";
    public int minutos = 0;
    public string minString = "00";

    public Button botonPlay;
    public Button botonRestart;
    public TextMeshProUGUI texto;

    private bool _pressed = false;


	void Start () {
		botonPlay.onClick.AddListener(Pressed);
        botonRestart.onClick.AddListener(PressedRestart);
	}

	void Pressed(){
		_pressed = true;
	}

    void PressedRestart () {
        timer = 0;
        segundos = "00";
        minutos = 0;
        minString = "00";
    }
    
    void Update()
    {
        if (_pressed) {
            if (timer >= 60f) {
                timer = 0;
                segundos = "00";
                minutos = minutos+1; 
                if (minutos < 10) {
                    minString = "0"+minutos;
                } else {
                    minString = minutos.ToString();
                }
            } else {
                timer += Time.deltaTime;
                if (timer < 10f) {
                    segundos =  "0" + (int) timer;
                } else {
                    segundos = ""+(int) timer;
                }
            }
        }

        texto.text = "Time\n" + minString + ":" + segundos;

    }

    private void OnTriggerEnter(Collider other) {
        _pressed = false;
    }
}
