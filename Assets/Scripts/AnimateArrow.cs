using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AnimateArrow : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        transform.DOLocalMove(new Vector3(0.215f,0.138f,-1.199f), 1.5f).SetEase(Ease.InOutSine).SetLoops(-1, LoopType.Yoyo);
    }
}
