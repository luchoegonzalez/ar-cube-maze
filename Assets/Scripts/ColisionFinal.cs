using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ColisionFinal : MonoBehaviour
{
    [SerializeField] private GameObject _texto;
    [SerializeField] private GameObject _restartBoton;
    [SerializeField] private Material _matLaberinto;
    [SerializeField] private GameObject _bola;
    [SerializeField] private Color _color;
    [SerializeField] private float _tiempo = 1;

    private void OnTriggerEnter(Collider other) {
        _texto.SetActive(true);
        _restartBoton.SetActive(true);
        _matLaberinto.DOColor(_color, _tiempo);
        _bola.SetActive(false);
    }
}
